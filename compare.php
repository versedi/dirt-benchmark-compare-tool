<?php 
include 'vendor/autoload.php';
function arrayRecursiveDiff($aArray1, $aArray2) {
  $aReturn = array();

  foreach ($aArray1 as $mKey => $mValue) {
    if (array_key_exists($mKey, $aArray2)) {
      if (is_array($mValue)) {
        $aRecursiveDiff = arrayRecursiveDiff($mValue, $aArray2[$mKey]);
        if (count($aRecursiveDiff)) { $aReturn[$mKey] = $aRecursiveDiff; }
      } else {
        if ($mValue != $aArray2[$mKey]) {
          $aReturn[$mKey] = $mValue;
        }
      }
    } else {
      $aReturn[$mKey] = $mValue;
    }
  }
  return $aReturn;
} 
function valueChanged($value)
{
    if(is_array($value)) {
        if(array_key_exists(0, $value) && array_key_exists(1, $value)) {
            if($value[0] != $value[1]) {
                return true;
            }
        }
    }
}
$service = new Sabre\Xml\Service();
$xml = $_POST['first'];
$xml2 = $_POST['second'];
$_first = array();
$_second = array();

$array = $service->parse($xml);
$array2 = $service->parse($xml2);

$totals = $array[0];
$fps_race = $array[1];
$fps_total = $array[2];
$track_details = $array[3];
$hardware_settings_config = $array[4];

$totals2 = $array2[0];
$fps_race2 = $array2[1];
$fps_total2 = $array2[2];
$track_details2 = $array2[3];
$hardware_settings_config2 = $array2[4];
$multilevelArrays = array('{}cpu','{}audio_card', '{}graphics_card');
$totalsConnected = array_merge_recursive($totals, $totals2);

// $fpsConnected = array_merge_recursive($fps_race, $fps_race2);

$_first['fps'] = $fps_race['attributes'];
$_second['fps'] = $fps_race2['attributes'];

$_first['track'] = $track_details['attributes'];
$_second['track'] = $track_details2['attributes'];

$_first['track'] = $totals['attributes'];
$_second['track'] = $totals2['attributes'];


$trackDetailsCon = array_merge_recursive($track_details, $track_details2);
// var_dump($trackDetailsCon);




// foreach($fpsConnected['attributes'] as $fpsMeasures) {

// }
// var_dump($totalsConnected);
// var_dump($hardware_settings_config2);
foreach($hardware_settings_config['value'] as $setting) {
    if(!in_array($setting['name'], $multilevelArrays)) {
        // var_dump($setting['name']);
        if(!is_null($setting['value'])) {
            $_first[$setting['name']] = $setting['value'];
        } else {
            $_first[$setting['name']] = $setting['attributes'];
        }
    } elseif($setting['name'] != '{}cpu') {
        // var_dump($setting);
        if(count($setting['value'] > 0)) {
            foreach($setting['value'] as $subSetting) {
                foreach($subSetting['attributes'] as $attribute => $attributeValue) {
                    if($attributeValue == '') {
                        $attributeValue = 'Values not set.';
                    }
                    $_first[$subSetting['name']][$attribute] = $attributeValue;
                }
                // var_dump($subSetting);
            }
        } 
        if(count($setting['attributes']) > 0) {
            foreach($setting['attributes'] as $attributeName => $attributeValue) {
                if($attributeValue == '') {
                    $attributeValue = 'Values not set.';
                }
                $_first[$attributeName] = $attributeValue;
                // var_dump($attributeName);
                // var_dump($attributeValue);
            }

        }
    } elseif($setting['name'] = '{}cpu') { 
        foreach($setting['value'] as $value) {
            $settingName = $value['name'];
            
            foreach($value as $avalue) {
                if(is_array($avalue)) {
                    foreach($avalue as $avaluename => $avaluee) {
                        $_first[$settingName][$avaluename] = $avaluee;
                    }
                }
            }
        }
    }

}
foreach($hardware_settings_config2['value'] as $setting) {
    if(!in_array($setting['name'], $multilevelArrays)) {
        if(!is_null($setting['value'])) {
            $_second[$setting['name']] = $setting['value'];
        } else {
            $_second[$setting['name']] = $setting['attributes'];
        }
    } elseif($setting['name'] != '{}cpu') {
        // var_dump($setting);

        if(count($setting['value'] > 0)) {
            foreach($setting['value'] as $subSetting) {
                foreach($subSetting['attributes'] as $attribute => $attributeValue) {
                    if($attributeValue == '') {
                        $attributeValue = 'Values not set.';
                    }
                    $_second[$subSetting['name']][$attribute] = $attributeValue;
                }
            }
        } 
        if(count($setting['attributes']) > 0) {
            foreach($setting['attributes'] as $attributeName => $attributeValue) {
                if($attributeValue == '') {
                    $attributeValue = 'Values not set.';
                }
                $_second[$attributeName] = $attributeValue;
                // var_dump($attributeName);
                // var_dump($attributeValue);
            }

        }
    } elseif($setting['name'] = '{}cpu') { 
        foreach($setting['value'] as $value) {
            $settingName = $value['name'];
            
            foreach($value as $avalue) {
                if(is_array($avalue)) {
                    foreach($avalue as $avaluename => $avaluee) {
                        $_second[$settingName][$avaluename] = $avaluee;
                    }
                }
            }
        }
    }

}
$connected = array_merge_recursive($_first, $_second);
$data = array();
$changes = array();
// var_dump($connected);
echo '<table>';
echo '<thead>';

echo '</thead>';
if(is_array($connected)) {
    foreach($connected as $settingName => $values) {
        if($settingName != 'rating' && $settingName != 'firstBoot') :
        echo '<tr><th>'.$settingName.'</th>';
            foreach($values as $setting => $settingValue) {
                if(valueChanged($settingValue)) {
                    echo '<td style="background-color:red;">'.$setting . ': '. $settingValue[0] .' / '. $settingValue[1] . '</td>';
                } else {
                    echo '<td>'.$setting . ': ' . $settingValue[0] .' / '. $settingValue[1] . '</td>';
                }
            }
            echo '</tr>';
        else:
            echo '<tr><th>'.$settingName.'</th>';
            if(valueChanged($values)) {
                echo '<td style="background-color:red;">'.$setting . ': '. $values[0] .' / '. $values[1] . '</td>';
            } else {
                echo '<td>'.$setting . ': '. $values[0] .' / '. $values[1] . '</td>';
            }
            echo '</tr>';
        endif;
    }
}


echo '</table>';